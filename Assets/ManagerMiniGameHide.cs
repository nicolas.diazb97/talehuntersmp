﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;


public class ManagerMiniGameHide : Singleton<ManagerMiniGameHide>
{

    public Text operation_;
    public Text tower_;
    public Text castle_;
    public Text pit_;
    public Text rock_;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void InstantiateObj(string _operation, int _tower, int _castle, int _pit, int _rock)
    {
        operation_.text = _operation;
        tower_.text = _tower.ToString();
        castle_.text = _castle.ToString();
        pit_.text = _pit.ToString();
        rock_.text = _rock.ToString();
        
        Debug.Log("Estoy llegando hasta instanciar las putas variables");
        List<MinigameItem> objs = GetComponentsInChildren<MinigameItem>(true).ToList();
        this.gameObject.SetActive(true);
        objs.ForEach(obj => obj.gameObject.SetActive(true));
        Debug.Log("Estoy llegando hasta encender el mini juego");
    }
}
