﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiPlayer : Singleton<UiPlayer>
{
    Text playerTurn;
    Button dice;
    public GameObject holder;
    public GameObject Uidice;
    private void Start()
    {
        playerTurn = GetComponentInChildren<Text>(true);
        dice = GetComponentInChildren<Button>(true);
    }
    public void StartTurn(string _playerTurn, bool isActive)
    {
        holder.SetActive(true);
        playerTurn.text = _playerTurn;
        dice.interactable = isActive;
        Uidice.SetActive(isActive);
    }
    public void StartTurn(bool isActive)
    {
        dice.interactable = isActive;
        Uidice.SetActive(isActive);
    }
    public void EndTurn()
    {
        dice.interactable = false;
    }
    public void ShowDice(string number)
    {
        dice.gameObject.GetComponentInChildren<Text>().text = number;
    }
}
