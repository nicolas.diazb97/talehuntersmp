﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DiceManager2 : MonoBehaviour
{
    public int currDiceValue = 0;
    public MovementManager2 movementManager;
    public TextMeshProUGUI diceText;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    public void RollDice()
    {
        currDiceValue = Random.Range(1, 7);
        movementManager.JumpAnimation();
        diceText.text = currDiceValue.ToString();
    }
}
