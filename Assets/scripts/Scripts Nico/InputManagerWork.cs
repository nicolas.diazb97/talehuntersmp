﻿using System.Collections;
using UnityEngine;

public enum SwipeWork { None, Up, Down, Left, Right };

public class InputManagerWork : MonoBehaviour
{
    public float minSwipeLength = 200f;
    Vector2 firstPressPos;
    Vector2 secondPressPos;
    Vector2 currentSwipe;
    Vector2 oldSwipe;
    public GameObject cube;
    public GameObject pointViewTarget;
    MovementManager movementManager;
    public static SwipeWork swipeDirection;
    public Camera cam;
    bool onAnimationEnded = true;

    private void Start()
    {
        movementManager = GetComponent<MovementManager>();
    }
    void Update()
    {
        DetectSwipe();
    }

    public void DetectSwipe()
    {
        if (Input.touches.Length > 0)
        {
            Touch t = Input.GetTouch(0);

            if (t.phase == TouchPhase.Began)
            {
                firstPressPos = new Vector2(t.position.x, t.position.y);
            }

            if (t.phase == TouchPhase.Ended)
            {
                secondPressPos = new Vector2(t.position.x, t.position.y);
                oldSwipe = new Vector3(secondPressPos.x - firstPressPos.x, secondPressPos.y - firstPressPos.y);
                float angle = 0.2618f;
                float xPrime = oldSwipe.x * Mathf.Cos(angle) - oldSwipe.y * Mathf.Sin(angle);
                float yPrime = oldSwipe.x * Mathf.Sin(angle) + oldSwipe.y * Mathf.Cos(angle);
                currentSwipe = new Vector2(xPrime, yPrime);

                // HACER SWIPE Y NO TAP
                if (currentSwipe.magnitude < minSwipeLength)
                {
                    swipeDirection = SwipeWork.None;
                    Debug.Log("TAP");
                    Ray ray = cam.ScreenPointToRay(Input.mousePosition);
                    RaycastHit hit;
                    Physics.Raycast(ray, out hit);
                    if (Input.GetTouch(0).position.x > Screen.width / 2)
                    {
                        Debug.Log("right");
                    }
                    else
                    {
                        Debug.Log("left");
                    }
                    //movementManager.SetDirectionState(new Directionated(), hit.point);
                    //Debug.Log(cam.ScreenToWorldPoint(Input.mousePosition));
                    return;
                }
                /*
                    m_MyQuaternion.SetFromToRotation(transform.position, m_MousePosition);
       
                    transform.rotation = m_MyQuaternion * transform.rotation;
                 */
                currentSwipe.Normalize();
                Quaternion m_MyQuaternion = Quaternion.identity;

                if (currentSwipe.y > 0 && currentSwipe.x > -0.5f && currentSwipe.x < 0.5f)
                {
                    Debug.Log("up");
                    swipeDirection = SwipeWork.Up;
                    m_MyQuaternion.SetFromToRotation(-Vector3.forward, Vector3.up);
                    if (onAnimationEnded)
                        StartCoroutine(Rotation(cube.transform, m_MyQuaternion, 0.5f));
                }
                else if (currentSwipe.y < 0 && currentSwipe.x > -0.5f && currentSwipe.x < 0.5f)
                {
                    Debug.Log("down");
                    swipeDirection = SwipeWork.Down;
                    m_MyQuaternion.SetFromToRotation(-Vector3.forward, Vector3.down);
                    if (onAnimationEnded)
                        StartCoroutine(Rotation(cube.transform, m_MyQuaternion, 0.5f));
                }
                else if (currentSwipe.x < 0 && currentSwipe.y > -0.5f && currentSwipe.y < 0.5f)
                {
                    Debug.Log("left");
                    swipeDirection = SwipeWork.Left;
                    m_MyQuaternion.SetFromToRotation(-Vector3.forward, Vector3.left);
                    if (onAnimationEnded)
                        StartCoroutine(Rotation(cube.transform, m_MyQuaternion, 0.5f));
                }
                else if (currentSwipe.x > 0 && currentSwipe.y > -0.5f && currentSwipe.y < 0.5f)
                {
                    Debug.Log("right");
                    swipeDirection = SwipeWork.Right;
                    m_MyQuaternion.SetFromToRotation(-Vector3.forward, Vector3.right);
                    if (onAnimationEnded)
                        StartCoroutine(Rotation(cube.transform, m_MyQuaternion, 0.5f));
                }
            }
        }
        else
        {
            swipeDirection = SwipeWork.None;
        }
    }
    public IEnumerator Rotation(Transform thisTransform, Quaternion degrees, float time)
    {
        Quaternion startRotation = thisTransform.rotation;
        Quaternion endRotation =degrees * thisTransform.rotation /** Quaternion.Euler(degrees)*/;
        float rate = 1.0f / time;
        float t = 0.0f;
        while (t < 1.0f)
        {
            onAnimationEnded = false;
            t += Time.deltaTime * rate;
            thisTransform.LookAt(pointViewTarget.transform.position );

            thisTransform.rotation = Quaternion.Slerp(startRotation, endRotation, t);
            yield return null;
        }
        Debug.Log("termino");
        onAnimationEnded = true;
    }
}