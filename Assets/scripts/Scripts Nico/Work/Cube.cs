﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cube : MonoBehaviour
{
    public Vector3 position;
    GameObject cubeObj;
    public GameObject cube;
    public Cube(Transform transformParent, Vector3 _position, GameObject _obj)
    {
        cubeObj = _obj;
        position = _position;
        cube = Instantiate(cubeObj, position, new Quaternion(0, 0, 0, 0), transformParent);
        cube.GetComponent<Wood>().cubeInstance = this;

    }

    internal void Draw()
    {
        cube.GetComponent<Wood>().active = true;
        if (!cube.GetComponent<Wood>().onFigure)
        {
            //cube.SetActive(false);
            //cube.GetComponent<Wood>().active = false;

            //cube.SetActive(false);
            cube.GetComponent<MeshRenderer>().materials[1].SetColor("_BaseColor", new Color32(255, 255, 255, 255));
            //cube.GetComponent<MeshRenderer>().materials[1].SetColor("_BaseColor", new Color32(58, 255, 0, 160));
        }
        else
        {
            cube.GetComponent<Wood>().onFigure = true;
        }
    }

    internal void Erase()
    {
        if (!cube.GetComponent<Wood>().onFigure)
        {

            //cube.GetComponent<MeshRenderer>().materials[1].SetColor("_BaseColor", new Color32(255, 255, 255, 255));
            cube.GetComponent<MeshRenderer>().materials[1].SetColor("_BaseColor", new Color32(58, 255, 0, 255));
        }
        else
        {
        }
    }
}
