﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Points : MonoBehaviour
{
    public List<PathPoint> points = new List<PathPoint>();
    void Start()
    {
        points = GetComponentsInChildren<PathPoint>().ToList(); 
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
