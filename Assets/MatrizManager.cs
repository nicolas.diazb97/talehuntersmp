using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MatrizManager : MonoBehaviour
{
    public Image background;
    public Image level;
    public Image map;
    public Sprite[] matrizSprites;
    public Sprite[] matrizLevels;
    public Sprite[] matrizMaps;
    public int currLevel;
    public int currLevelType;
    public int currLevelMap;
    void Start()
    {
        Debug.Log(matrizSprites.Length);
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void SelectNextLevel()
    {
        currLevel = currLevel + 1;
        if (currLevel == matrizSprites.Length)
        {
            currLevel = 0;
        }
        background.GetComponent<Image>().sprite = matrizSprites[currLevel];
    }

    public void SelectPreviousLevel()
    {
        if (currLevel == 0)
        {
            currLevel = matrizSprites.Length;
        }
        background.GetComponent<Image>().sprite = matrizSprites[currLevel - 1];
        currLevel = currLevel - 1;
        //se resto uno para obrtemner la pos max
    }

    public void SelectNextLevelType()
    {
        currLevelType = currLevelType + 1;
        if (currLevelType == matrizLevels.Length)
        {
            currLevelType = 0;
        }
        level.GetComponent<Image>().sprite = matrizLevels[currLevelType];
    }

    public void SelectPreviousLevelType()
    {
        if (currLevelType == 0)
        {
            currLevelType = matrizLevels.Length;
        }
        level.GetComponent<Image>().sprite = matrizLevels[currLevelType - 1];
        currLevelType = currLevelType - 1;
        //se resto uno para obrtemner la pos max
    }

    public void SelectNextLevelMap()
    {
        currLevelMap = currLevelMap + 1;
        if (currLevelMap == matrizMaps.Length)
        {
            currLevelMap = 0;
        }
        map.GetComponent<Image>().sprite = matrizMaps[currLevelMap];
    }

    public void SelectPreviousLevelMap()
    {
        if (currLevelMap == 0)
        {
            currLevelMap = matrizMaps.Length;
        }
        map.GetComponent<Image>().sprite = matrizMaps[currLevelMap - 1];
        currLevelMap = currLevelMap - 1;
        //se resto uno para obrtemner la pos max
    }

 
}
