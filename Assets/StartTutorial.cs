﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartTutorial : TutorialScreen
{
    int eventsCounter;
    Pablo pablo;
    // Start is called before the first frame update
    public override void Process()
    {
        pablo = FindObjectOfType<Pablo>();
        pablo.DisableMovement();
        if (eventsCounter <= 0)
        {
            this.gameObject.SetActive(true);
            eventsCounter++;
        }
        else
        {
            TutorialManager.main.NextScreen();
        }
    }
}