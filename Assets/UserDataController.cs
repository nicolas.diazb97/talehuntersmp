﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UserDataController : MonoBehaviour
{
    public RawImage profilePic;
    // Start is called before the first frame update
    void Start()
    {
        profilePic.texture = DataManager.main.GetProfilePic();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
