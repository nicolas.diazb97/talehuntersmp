﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FunctionOfButtons : MonoBehaviour
{
    public GameObject menuContainer;
    public Animator privateRoomContainer;
    public GameObject subMenu;
    public GameObject hostContainer;

    public void PrivateRoomOn()
    {
        menuContainer.SetActive(false);
        privateRoomContainer.Play("Grow");
    }
    public void SubMenuOff()
    {
        subMenu.SetActive(false);
        hostContainer.SetActive(false);
    }
}
