﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class MemoRyMinigame : Singleton<MemoRyMinigame>
{
    public float playerAnswer = 0;
    public Text answerText;
    public GameObject objToInstatiate;
    public DropSlot[,] slotPositions = new DropSlot[4,3];
    public List<MemoryMingame> cards;
    public DropSlot[] posX;
    public DropSlot[] posX1;
    public DropSlot[] posX2;
    public DropSlot[] posX3;
    // Start is called before the first frame update
    void Start()
    {
        //InstantiateObj(46);
        //FillCardsSlots();
        FillSlotsPositions();
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    //public void FillCardsSlots()
    //{
    //    cards = GetComponentsInChildren<MemoryMingame>(true).ToList();
    //}

    public void FillSlotsPositions()
    {
        for (int i = 0; i < 3; i++)
        {
            slotPositions[0, i] = posX[i];
            slotPositions[1, i] = posX1[i];
            slotPositions[2, i] = posX2[i];
            slotPositions[3, i] = posX3[i];
        }

        Debug.Log("SlotPosition" + slotPositions[0, 0]);
        Debug.Log("SlotPosition 2" + slotPositions[2, 0].name);
    }

    public void InstantiateObj(int[,] _count)
    {
        List<MinigameItem> objs = GetComponentsInChildren<MinigameItem>(true).ToList();
        this.gameObject.SetActive(true);
        objs.ForEach(obj => obj.gameObject.SetActive(true));
        FillSlotsPositions();

        for (int i = 0; i < 4; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                Debug.Log("Count"+_count[i, j]);
                Debug.Log("Slot"+slotPositions[i, j].name);
                slotPositions[i, j].GetComponent<Image>().sprite = cards.Where(card => card.id == _count[i, j]).ToList()[0].GetComponent<Image>().sprite;
            }
        }
        StartCoroutine(WaitToClear());
    }

    public IEnumerator WaitToClear()
    {
        yield return new WaitForSecondsRealtime(15f);
        for (int i = 0; i < 4; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                slotPositions[i, j].GetComponent<Image>().sprite = null;
            }
        }
    }

    public void EndCountMiniGame()
    {
        Debug.Log("Entro al metodo EndCountMiniGame");
        this.gameObject.SetActive(false);
        List<MinigameItem> objs = GetComponentsInChildren<MinigameItem>(true).ToList();
        objs.ForEach(obj => obj.gameObject.SetActive(false));
    }
}
