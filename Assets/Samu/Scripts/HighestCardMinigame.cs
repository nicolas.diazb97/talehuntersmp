﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HighestCardMinigame : MonoBehaviour
{
    public float card1Value;
    public float card2Value;
    public float card3Value;
    public float card4Value;
    public float card5Value;
    public float card6Value;

    public Text card1;
    public Text card2;
    public Text card3;
    public Text card4;
    public Text card5;
    public Text card6;
    void Start()
    {
        NumbersAssignation();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void NumbersAssignation()
    {
        card1Value = Random.Range(1, 3);
        card2Value = Random.Range(2, 4);
        card3Value = Random.Range(3, 4);
        card4Value = Random.Range(3, 5);
        card5Value = Random.Range(4, 6);
        card6Value = 5;

        card1.text = card1Value.ToString();
        card2.text = card2Value.ToString();
        card3.text = card3Value.ToString();
        card4.text = card4Value.ToString();
        card5.text = card5Value.ToString();
        card6.text = card6Value.ToString();
    }
}
