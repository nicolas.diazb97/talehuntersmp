﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinigamePosition : MonoBehaviour
{
    public Minigame playedMinigame;
    public int position;

    public MinigamePosition(Minigame _playedMinigame, int _position)
    {
        playedMinigame = _playedMinigame;
        position = _position;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
