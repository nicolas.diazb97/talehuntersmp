﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public string playerId;
    public string playerName;
    public string source;
    public bool currPlayer;
    public Texture2D profilePic;
    public PlayerRef refToPlayer;
    public int consecutiveId;
    // Start is called before the first frame update
    public List<MinigamePosition> positions;

    private void Start()
    {
        positions = new List<MinigamePosition>();
    }
    public Player(string _playerName, string _id, bool _currPlayer, Texture2D _profilePic, int _conId)
    {
        playerName = _playerName;
        playerId = _id;
        currPlayer = _currPlayer;
        profilePic = _profilePic;
        consecutiveId = _conId;
    }
    public void init(string _playerName, string _id, bool _currPlayer, Texture2D _profilePic, int _conId)
    {
        playerName = _playerName;
        playerId = _id;
        currPlayer = _currPlayer;
        profilePic = _profilePic;
        consecutiveId = _conId;
    }

    public void SetPostion(MinigamePosition position_)
    {
        positions.Add(position_);
    }

    public int GetGamePositionByType(Minigame _minigame)
    {
        List<MinigamePosition> matchedMinigames = positions.Where(position => position.playedMinigame.GetType() == _minigame.GetType()).ToList();
        if(matchedMinigames.Count() > 0)
        {
            return matchedMinigames[0].position;
        }
        else
        {
            return 404;
        }
    }

    public void SetGamePositionByType(Minigame _minigame)
    {
        //List<MinigamePosition> matchedMinigames = positions.Where(position => position.playedMinigame.GetType() == _minigame.GetType()).ToList();
        //if (matchedMinigames.Count() > 0)
        //{
        //    return matchedMinigames[0].position;
        //}
        //else
        //{
        //    return 404;
        //}
    }
}
