﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class PlayerManager : Singleton<PlayerManager>
{
    public GameObject playerChipPrefab;
    public Transform[] playerChipReference;
    int playersCounter = 0;
    public List<Player> playersInGame = new List<Player>();
    public GameObject playerPrefab;
    public GameObject playersContainer;
    public Color[] chipColors;
    public GameObject playerOnTurn;
    public int currId;

    public List<GameResult> results;
    public Transform[] spawnPositions;
    private void Start()
    {
        results = new List<GameResult>();
    }
    public void InitReferences(int _index)
    {
        playerChipReference = spawnPositions[_index].GetComponentsInChildren<Transform>() ;
    }
    public void PlayAnimation(string _playerId, string _animation)
    {
        Debug.Log("player: " + _playerId + " anim: " + _animation);
        GetPlayerById(_playerId).refToPlayer.GetComponentInChildren<Animator>().SetTrigger(_animation);
    }
    public void SetCurrConsecutiveId(string _playerid)
    {
        currId = GetPlayerById(_playerid).consecutiveId;
    }
    public void CreatePlayer(string nameId, string playerId, bool activePlayer, Texture2D profilePic)
    {
        GameObject newP = Instantiate(playerPrefab);
        GameObject newPlayerChip = Instantiate(playerChipPrefab, playerChipReference[playersCounter].transform.position, playerChipReference[playersCounter].transform.rotation, DiceManager.main.transform);
        List<ChipMaterial> rend = newPlayerChip.GetComponentsInChildren<ChipMaterial>().ToList();
        rend.ForEach(r =>
        {
            // You can re-use this block between calls rather than constructing a new one each time.
            var block = new MaterialPropertyBlock();

            // You can look up the property by ID i[nstead of the string to be more efficient.
            block.SetColor("_BaseColor", chipColors[playersCounter]);

            // You can cache a reference to the renderer to avoid searching for it.
            r.GetComponent<Renderer>().SetPropertyBlock(block);
        });
        //Debug.LogError(FindObjectOfType<SocketManager>().currLevel+" count: "+ newPlayerChip.GetComponent<PlayerRef>().playableModels.Count()+ newPlayerChip.GetComponent<PlayerRef>().name);
        newPlayerChip.GetComponent<PlayerRef>().playableModels[FindObjectOfType<SocketManager>().currLevel].GetComponentsInChildren<PlayerChip>(true).ToList()[playersCounter].gameObject.SetActive(true);
        DiceMatManager.main.SetDiceMat(FindObjectOfType<SocketManager>().currLevel);
        newP.AddComponent(typeof(Player));
        newP.GetComponent<Player>().init(nameId, playerId, activePlayer, profilePic, playersCounter);
        newPlayerChip.GetComponent<PlayerRef>().Init(0, newP.GetComponent<Player>());
        newP.transform.SetParent(playersContainer.transform);
        newP.transform.localScale = new Vector3(2.17f, 2.17f, 2.17f);
        newP.GetComponentInChildren<Text>().text = nameId;
        newP.GetComponentInChildren<RawImage>().texture = profilePic;
        playersInGame.Add(newP.GetComponent<Player>());
        newP.GetComponent<Player>().refToPlayer = newPlayerChip.GetComponent<PlayerRef>();
        playersCounter++;
    }
    public bool PlayerAlreadyExists(string playerId)
    {
        bool inPool = false;
        playersInGame.ForEach(p =>
        {
            if (p.playerId == playerId)
                inPool = true;
        });
        return inPool;
    }
    public Player GetPlayerById(string id)
    {
        return playersInGame.Where(p => p.playerId == id).ToArray()[0];
    }
    public Player GetPlayerByConsecutiveId(int id)
    {
        Debug.LogError("surrid" + id);
        if (playersInGame.Where(p => p.consecutiveId == id).ToList().Count > 0)
            return playersInGame.Where(p => p.consecutiveId == id).ToArray()[0];
        else
            return null;
    }
    public Player GetPlayerUser(string id)
    {
        return playersInGame.Where(p => p.playerId == id).ToArray()[0];
    }
    public void UpdatePlayerId(Player player, string newId)
    {
        player.playerId = newId;
    }

    public GameResult SetCurrentGamePositions(Minigame _minigame)
    {
        bool isQueryReady = false;
        GameResult tempResult = new GameResult();
        string[] playersPosition = new string[playersInGame.Count()];
        playersInGame.ForEach(playerInGame =>
        {
            Debug.LogError("Funciona we " + playerInGame.GetGamePositionByType(_minigame));
            playersPosition[playerInGame.GetGamePositionByType(_minigame)-1] = playerInGame.playerName;
            if (playerInGame.GetGamePositionByType(_minigame) == playersPosition.Count())
            {
                isQueryReady = true;
            }
        });
        while (!isQueryReady)
        {
        }
        tempResult.results = playersPosition;
        tempResult.currMinigame = _minigame;
        return tempResult;
    }
}
