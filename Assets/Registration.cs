﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Registration : MonoBehaviour
{
    public Text error;
    public InputField email;
    public InputField password;
    public InputField nickname;
    public GameObject popUp;
    public Texture2D defaultPic;
    bool registFinished;
    bool userFounded;
    // Start is called before the first frame update
    public void TryToRegist()
    {
        Regist regCallback = new Regist();
        if (CheckForEmptySpaces())
        {
            regCallback.OnUserSearchOver.AddListener((bool t) =>
            {
                UserSearchOver(t, email.text, password.text, nickname.text);
            });
            LoginManager.main.FindRegisteredUser(email.text, password.text, regCallback);
        }
        else
        {
            error.text = "*Rellene todos los campos";
        }
    }
    private void Update()
    {
        if (registFinished)
            ChangeScene();
        if (userFounded)
        {
            error.text = "El usuario ya existe";
        }

    }
    public void ChangeScene()
    {
        DataManager.main.SetProfilePic(defaultPic);
        DataManager.main.SetUserName(nickname.text);
        popUp.SetActive(true);
        popUp.GetComponentInChildren<Text>().text = "¡Bienvenid@ " + nickname.text + "!";
        Debug.Log("cambio");
        //SceneManager.LoadScene("ChooseLogged");
        //Debug.Log("cambio2");
    }
    public bool CheckForEmptySpaces()
    {
        if (email.text != "" && password.text != "" && nickname.text != "")
            return true;
        else
            return false;
    }
    private void UserSearchOver(bool userExists, string _email, string _password, string _nickname)
    {
        Debug.Log("busqueda finalizada");
        if (userExists)
        {
            userFounded = true;
        }
        else
        {
            LoginManager.main.WriteNewUser(_password, _nickname, _email);
            registFinished = true;
        }
    }
}
