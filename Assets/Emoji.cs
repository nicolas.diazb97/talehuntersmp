﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Emoji
{
    // emojis ids:
    // happy: 0
    // sad: 1
    // kiss: 2
    [SerializeField]
    public int emojiId;
    [SerializeField]
    public string playerId;
    public Emoji(int _emojiId, string _playerId)
    {
        emojiId = _emojiId;
        playerId = _playerId;
    }
}
