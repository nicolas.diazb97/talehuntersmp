﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class BallotsManager : Singleton<BallotsManager>
{
    public GameObject ballotPrefab;
    public AudioManager audioManager;
    public Transform ballotReference;
    public List<Ballot> ballots = new List<Ballot>();
    public List<BallotAudioClip> clips = new List<BallotAudioClip>();
    public AudioSource audioSource;
    // Start is called before the first frame update
    private void Start()
    {
        audioManager = FindObjectOfType<AudioManager>();
        clips = audioManager.GetComponentsInChildren<BallotAudioClip>().ToList();
    }
    public void NewBallot(string letter, string number)
    {
        GameObject tempBallot = Instantiate(ballotPrefab, ballotReference.position, ballotReference.rotation, this.transform);
        tempBallot.GetComponentInChildren<BallotLetter>().GetComponentInChildren<Text>().text = letter;
        tempBallot.GetComponentInChildren<BallotNumber>().GetComponentInChildren<Text>().text = number;
        ballots.Add(tempBallot.GetComponent<Ballot>());
        BallotsNextStep(tempBallot.GetComponent<Ballot>());
    }
    public void BallotsNextStep(Ballot exception)
    {
        ballots.ForEach(b =>
        {
            if (b != exception)
            {
                b.GetComponent<Animator>().SetTrigger("newBallot");
            }
        });
    }
    public void PlaySound(string id)
    {
        Debug.Log(id);
        if (clips.Where(p => p.id == id).ToArray()[0])
            audioSource.PlayOneShot(clips.Where(p => p.id == id).ToArray()[0].clip);
        else
            Debug.LogError("no se encontro el clip");
    }
}
