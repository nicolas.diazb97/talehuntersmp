﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class resultGameCount 
{
    [SerializeField]
    public string playerId;
    [SerializeField]
    public int result;
    [SerializeField]
    public int position;

    //public resultGameCount(string playerId_, int result_, int postion_)
    //{
    //    playerId = playerId_;
    //    result = result_;
    //    position = postion_;
    //}
}
