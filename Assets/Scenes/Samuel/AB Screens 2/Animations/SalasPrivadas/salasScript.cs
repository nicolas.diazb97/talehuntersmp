﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class salasScript : MonoBehaviour
{
    public GameObject cancelButton;
    public GameObject popUpAceptado;
    public GameObject leaveButton;
    public GameObject estadoText;
    public GameObject popUpLeave;
    public GameObject popUpLeave2;

    public Animator objects;
    public Animator maskUsers;

    public void OpenPopUpAceptado()
    {
        cancelButton.SetActive(false);
        popUpAceptado.SetActive(true);
        leaveButton.SetActive(true);
        estadoText.SetActive(false);
    }

    public void expandObjects()
    {
        popUpAceptado.SetActive(false);
        objects.Play("Expand");
    }

    public void NoClosePopUp()
    {
        popUpLeave.SetActive(false);
    }

    public void LeavePopUp()
    {
        popUpLeave.SetActive(true);
    }

    public void ClosePopUp()
    {
        popUpLeave.SetActive(false);
        maskUsers.Play("Shrink");
    }

    public void ClosePopUp2()
    {
        popUpLeave.SetActive(false);
    }
}
