﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationsEvents : MonoBehaviour
{
    public GameObject ContenidoEmail;
    public GameObject ContenidoMarketing;
    public GameObject ContenidoInventario;
    public GameObject ContenidoEmpleados;
    public GameObject ContenidoMejoras;
    public GameObject ContenidoSalas;

    public void Marketing()
    {
        ContenidoMarketing.SetActive(true);
        ContenidoInventario.SetActive(false);
        ContenidoEmpleados.SetActive(false);
        ContenidoMejoras.SetActive(false);
        ContenidoEmail.SetActive(false);
        ContenidoSalas.SetActive(false);
    }

    public void Inventario()
    {
        ContenidoInventario.SetActive(true);
        ContenidoMarketing.SetActive(false);
        ContenidoEmpleados.SetActive(false);
        ContenidoMejoras.SetActive(false);
        ContenidoEmail.SetActive(false);
        ContenidoSalas.SetActive(false);
    }

    public void Empleados()
    {
        ContenidoEmpleados.SetActive(true);
        ContenidoMarketing.SetActive(false);
        ContenidoInventario.SetActive(false);
        ContenidoMejoras.SetActive(false);
        ContenidoEmail.SetActive(false);
        ContenidoSalas.SetActive(false);
    }

    public void Mejoras()
    {
        ContenidoMejoras.SetActive(true);
        ContenidoMarketing.SetActive(false);
        ContenidoInventario.SetActive(false);
        ContenidoEmpleados.SetActive(false);
        ContenidoEmail.SetActive(false);
        ContenidoSalas.SetActive(false);
    }

    public void Correo()
    {
        ContenidoEmail.SetActive(true);
        ContenidoMarketing.SetActive(false);
        ContenidoInventario.SetActive(false);
        ContenidoEmpleados.SetActive(false);
        ContenidoMejoras.SetActive(false);
        ContenidoSalas.SetActive(false);
    }

    public void Salas()
    {
        ContenidoSalas.SetActive(true);
        ContenidoEmail.SetActive(false);
        ContenidoMarketing.SetActive(false);
        ContenidoInventario.SetActive(false);
        ContenidoEmpleados.SetActive(false);
        ContenidoMejoras.SetActive(false);
    }
}
