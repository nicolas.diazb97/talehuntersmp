﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransicionEventos : MonoBehaviour
{
    public GameObject ContenidoEmail;
    public GameObject ContenidoMarketing;

    public void MarketingAparece()
    {
        ContenidoMarketing.SetActive(true);
    }
    
    public void MarketingDesaparece()
    {
        ContenidoMarketing.SetActive(false);
    }

    public void EmailAparece()
    {
        ContenidoEmail.SetActive(true);
    }

    public void EmailDesaparece()
    {
        ContenidoEmail.SetActive(false);
    }
}
