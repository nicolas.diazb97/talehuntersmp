﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopUpsScript : MonoBehaviour
{
    public GameObject PopUpOmitir;
    public GameObject PopUpMejora;

    public void PopUpOmitirAparece()
    {
        PopUpOmitir.SetActive(true);
    }

    public void PopUpMejoraAparece()
    {
        PopUpMejora.SetActive(true);
    }

    public void PopUpMejoraDesaparece()
    {
        PopUpMejora.SetActive(false);
    }
}
