﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cross_Empleados_Events : MonoBehaviour
{
    public GameObject empleadoContratado;    
    public GameObject empleadoContratar;
    
    public void EmpleadoSeContrata()
    {
        empleadoContratado.SetActive(true);
        empleadoContratar.SetActive(false);
    }
}
