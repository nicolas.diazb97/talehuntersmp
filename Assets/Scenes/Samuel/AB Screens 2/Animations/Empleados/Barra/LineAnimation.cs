﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineAnimation : MonoBehaviour
{
    public GameObject ContratadosCamping;
    public GameObject ContratadosNeucha;
    public GameObject ContratarNeucha;
    public GameObject ContratarCamping;
    public Animator Line;
    public Animator SliderEmpleados;

    public void RightActionFonts()
    {
        ContratadosNeucha.SetActive(true);
        ContratadosCamping.SetActive(false);
        ContratarCamping.SetActive(true);
        ContratarNeucha.SetActive(false);

        Line.Play("Move Right");
        SliderEmpleados.Play("Move-To-Contratar");
    }
    public void LeftActionFonts()
    {
        ContratadosNeucha.SetActive(false);
        ContratadosCamping.SetActive(true);
        ContratarCamping.SetActive(false);
        ContratarNeucha.SetActive(true);

        Line.Play("Move Left");
        SliderEmpleados.Play("Move-To-Contratados");
    }

}
