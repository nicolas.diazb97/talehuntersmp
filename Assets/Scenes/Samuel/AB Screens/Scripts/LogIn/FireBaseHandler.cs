﻿using Facebook.Unity;
using Firebase.Auth;
using Firebase;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBaseHandler : Singleton<FireBaseHandler>
{
    public FirebaseAuth Auth => auth;
    private FirebaseAuth auth;

    // Start is called before the first frame update
    void Start()
    {
        var perms = new List<string>() { "public_profile", "email" };
        FB.LogInWithReadPermissions(perms, OnFacebookLoginResults);
    }

    private void OnFacebookLoginResults(ILoginResult result)
    {
        if (FB.IsLoggedIn)
        {
            var accesToken = AccessToken.CurrentAccessToken;
            SignInFirebase(accesToken);
        }
        else
        {
            //SignInAction(obj: false);
        }
    }

    private void SignInFirebase(AccessToken accesToken)
    {
        var credential = FacebookAuthProvider.GetCredential(accesToken.TokenString);
        auth.SignInWithCredentialAsync(credential).ContinueWith(continuationAction: task =>
        {
            if (task.IsCanceled)
            {
                Debug.Log(message: "sing in cancelled");
                //Signin
            }
        });
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void SignInFacebook()
    {

    }
}
