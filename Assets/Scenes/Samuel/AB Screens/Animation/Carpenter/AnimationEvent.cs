﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationEvent : MonoBehaviour
{
    public GameObject CarpenterDown;
    public GameObject Carpenter;
    public void Event()
    {
        Carpenter.SetActive(false);
        CarpenterDown.SetActive(true);

    }
}
