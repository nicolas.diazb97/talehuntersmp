﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TableScript : MonoBehaviour
{
    public GameObject popUp;
    public GameObject TableRelated;
    public GameObject ComputerRelated;
    public GameObject CloseThePopUp;
    public GameObject OnTable;
    public GameObject OffTable;

    public void PopUp()
    {
        popUp.SetActive(true);
        OnTable.SetActive(true);
        ComputerRelated.SetActive(false);
        TableRelated.SetActive(true);
    }

    public void ClosePopUp()
    {
        CloseThePopUp.SetActive(false);
        OffTable.SetActive(false);
    }
}
