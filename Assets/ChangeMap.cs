﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeMap : MonoBehaviour
{
    public GameObject camera1;
    public GameObject camera2;

    public bool onCamera2 =true;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void Interact()
    {
        if (onCamera2)
        {
            camera2.SetActive(false);
            camera1.SetActive(true);
            onCamera2 = false;
        }
        else
        {
            camera2.SetActive(true);
            camera1.SetActive(false);
            onCamera2 = true;
        }
    }
}
