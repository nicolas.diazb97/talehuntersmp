using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelCharacter : MonoBehaviour
{
    public GameObject[] matrizCharacter;
    public int currLevel;
    public GameObject[] matrizMaps;
    public int currLevelMap;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    public void SelectNextCharacter()
    {
        currLevel = currLevel + 1;
        if (currLevel == matrizCharacter.Length)
        {
            currLevel = 0;
            matrizCharacter[matrizCharacter.Length - 1].GetComponentInChildren<SkinnedMeshRenderer>(true).enabled = false;
            if (matrizCharacter[matrizCharacter.Length - 1].GetComponent<CharacterModel>())
            {
                Debug.LogError("kjasdhkjhasdkjhdsakjh");
                matrizCharacter[matrizCharacter.Length - 1].GetComponent<CharacterModel>().meshesOff.ForEach(v=>v.enabled = false);
            }
        }
        else
        {
            matrizCharacter[currLevel - 1].GetComponentInChildren<SkinnedMeshRenderer>(true).enabled = false;
            if (matrizCharacter[currLevel - 1].GetComponent<CharacterModel>())
            {
                Debug.LogError("kjasdhkjhasdkjhdsakjh");
                matrizCharacter[currLevel - 1].GetComponent<CharacterModel>().meshesOff.ForEach(v => v.enabled = false);
            }
        }
        matrizCharacter[currLevel].GetComponentInChildren<SkinnedMeshRenderer>(true).enabled = true;
        if (matrizCharacter[currLevel].GetComponent<CharacterModel>())
        {
                Debug.LogError("kjasdhkjhasdkjhdsakjh");
            matrizCharacter[currLevel].GetComponent<CharacterModel>().meshesOff.ForEach(v => v.enabled = true);
        }
        //character.SetActive(true);
    }

    public void SelectPreviousCharacter()
    {
        matrizCharacter[currLevel].GetComponentInChildren<MeshRenderer>().enabled = false;
        if (matrizCharacter[currLevel].GetComponent<CharacterModel>())
        {
            matrizCharacter[currLevel].GetComponent<CharacterModel>().meshesOff.ForEach(v => v.enabled = false);
        }
        if (currLevel == 0)
        {
            currLevel = matrizCharacter.Length;
        }
        matrizCharacter[currLevel - 1].GetComponentInChildren<MeshRenderer>().enabled = true;
        if (matrizCharacter[currLevel - 1].GetComponent<CharacterModel>())
        {
            matrizCharacter[currLevel - 1].GetComponent<CharacterModel>().meshesOff.ForEach(v => v.enabled = true);
        }
        currLevel = currLevel - 1;
        //se resto uno para obrtemner la pos max
    }

    public void SelectNextMap()
    {
        currLevelMap = currLevelMap + 1;
        if (currLevelMap == matrizMaps.Length)
        {
            currLevelMap = 0;
            matrizMaps[matrizMaps.Length - 1].SetActive(false);
        }
        else
        {
            matrizMaps[currLevelMap - 1].SetActive(false);
        }
        matrizMaps[currLevelMap].SetActive(true);


    }

    public void SelectPreviousMap()
    {
        matrizMaps[currLevelMap].SetActive(false);
        if (currLevelMap == 0)
        {
            currLevelMap = matrizMaps.Length;
        }
        matrizMaps[currLevelMap - 1].SetActive(true);
        currLevelMap = currLevelMap - 1;
        //se resto uno para obrtemner la pos max
    }

}
