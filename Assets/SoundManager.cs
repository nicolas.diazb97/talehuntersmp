﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public AudioClip SoundRio;
    public AudioSource rioaudio;

    private void Start()
    {
        rioaudio = GetComponent<AudioSource>();
    }
    private void OnTriggerEnter()
    {
        Debug.Log("prende sonido");
        rioaudio.Play();
    }
}
