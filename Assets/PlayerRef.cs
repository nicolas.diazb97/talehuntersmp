﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerRef : MonoBehaviour
{
    public int currSectionId;
    public Player player;
    public NavMeshAgent agent;
    public List<ChoosablePlayer> playableModels;
    public Transform playerTrans;
    public Transform playerMesh;
    public Transform nextPos;
    public Transform[] positions;
    public int currLevel;
    public DiceManager2 diceManager;

    public void Init(int _section, Player _player)
    {
        currSectionId = _section;
        player = _player;
        agent = GetComponent<NavMeshAgent>();
        DiceManager.main.RefreshPlayers();
    }
    // Start is called before the first frame update
    void Start()
    {
        playerTrans.transform.LookAt(positions[currLevel + 1]);

    }

    // Update is called once per frame
    void Update()
    {

    }
    public void MovePlayer(Transform aim)
    {
        if (aim)
        {

            agent.SetDestination(aim.position);
        }
    }
    public void HasArrived()
    {

    }
}
