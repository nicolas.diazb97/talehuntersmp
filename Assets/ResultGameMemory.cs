﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ResultGameMemory
{
    
    public string position1;
    public string position2;
    public string position3;
}
