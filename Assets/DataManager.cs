﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class DataManager : Singleton<DataManager>
{
    //public Text logCounter;
    Texture2D profilePic;
    string userName;
    string currSocketId;
    string pin;
    int connectionsCounter = 0;
    public DataToStore gameData;
    private float timePassed;

    // Start is called before the first frame update
  
    // Update is called once per frame
    public void UpdateGameData()
    {
        Debug.Log("UPDATED");
        Serializer.Save<DataToStore>(Application.persistentDataPath + "/GameData.cs", gameData);
    }
    private void Start()
    {
        gameData = Serializer.Load<DataToStore>(Application.persistentDataPath + "/GameData.cs");
        if (gameData != null)
        {
            GetLocalData();
        }
    }
    public void SaveGamePin(string _pin)
    {
        pin = _pin;
    }

    public Texture2D GetProfilePic()
    {
        return profilePic;
    }
    public string GetUserName()
    {
        return userName;
    }
    public void SetUserName(string _name)
    {
        userName= _name;
    }
    public void NewConnection()
    {
        connectionsCounter++;
        //logCounter.text = connectionsCounter.ToString();
    }
    public void SetProfilePic(Texture2D img)
    {
        Debug.Log("profile picture acquire");
        LoginManager.main.ReadyToStart();
        profilePic = img;
    }
    public void SetSocketId(string socketId)
    {
        currSocketId = socketId;
    }
    public string GetSocketId()
    {
        return currSocketId;
    }
    public bool CheckForRefreshedConnections()
    {
        if (connectionsCounter > 1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public void Log()
    {

        //logCounter.text = "refreshed";
    }
    public string GetGamePin()
    {
        return pin;
    }

    public void SaveCurrLocalData(bool playingGame)
    {
        Debug.Log("salvando");
        string stringData = Convert.ToBase64String(profilePic.EncodeToJPG()); //1
        DataToStore localData = new DataToStore(stringData, userName, currSocketId, playingGame);
        Serializer.Save<DataToStore>(Application.persistentDataPath + "/Gamedata.cs", localData);
    }
    public void GetLocalData()
    {
        DataToStore localData = Serializer.Load<DataToStore>(Application.persistentDataPath + "/Gamedata.cs");

        Texture2D newPhoto = new Texture2D(1, 1);
        newPhoto.LoadImage(Convert.FromBase64String(localData.image));
        newPhoto.Apply();
        profilePic = newPhoto;
        userName = localData.nameId;
        currSocketId = localData.currId;
        if (userName != "")
        {
            SceneManager.LoadScene("TestScene");
        }
    }
}
