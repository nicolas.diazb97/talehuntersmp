﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Serpiente : section
{
    public SocketManager socketManager;
    bool processed = false;


    public override void Init(GameObject _player)
    {
        playerToArrived = _player;
        searchForPlayer = true;
        socketManager = FindObjectOfType<SocketManager>();
    }

    public override void OnPlayerArrived()
    {
        SectionManager.main.PlayerHasArrived(this.GetComponent<section>());

    }

    public override void Process()
    {
        if (!processed)
        {
            UiManager.main.InitMiniGame(PlayerManager.main.GetPlayerById(playerToArrived.GetComponent<PlayerRef>().player.playerId));
            socketManager.CreateGameCount();
            processed = true;
        }
        Debug.Log("en math game");
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame

    // Update is called once per frame
    public override void Update()
    {
        base.Update();
    }
}
