﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dadosminijuego : MonoBehaviour
{
    public Animator dice1;
    public Animator sign;
    public float animationSpeed = 1;
    public bool number2 = false;
    public bool number3 = false;
    public bool number4 = false;
    public bool number5 = false;
    SocketManager socketManager;

    public float numberResult = 0;

    private void Start()
    {
        socketManager = FindObjectOfType<SocketManager>();
    }
    public void tryToStop()
    {
        socketManager.RollMiniGameDice();
    }
    public void StopDice()
    {
        animationSpeed= dice1.speed = 0;
        if (number2 == true && animationSpeed == 0)
        {
            numberResult = 2;
            dice1.Play("+Up");
            sign.Play("Grow");
        }
        if (number3 == true && animationSpeed == 0)
        {
            numberResult = 3;
            dice1.Play("-Up");
            sign.Play("Grow");
        }
        if (number4 == true && animationSpeed == 0)
        {
            numberResult = 4;
            dice1.Play("xUp");
            sign.Play("Grow");
        }
        if (number5 == true && animationSpeed == 0)
        {
            numberResult = 5;
            dice1.Play("dUp");
            sign.Play("Grow");
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Number2On()
    {
        number2 = true;
    }

    public void Number2Off()
    {
        number2 = false;
    }

    public void Number3On()
    {
        number3 = true;
    }

    public void Number3Off()
    {
        number3 = false;
    }

    public void Number4On()
    {
        number4 = true;
    }

    public void Number4Off()
    {
        number4 = false;
    }

    public void Number5On()
    {
        number5 = true;
    }

    public void Number5Off()
    {
        number5 = false;
    }


}
